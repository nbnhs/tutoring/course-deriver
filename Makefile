build: dep go

dep:
	dep ensure

go:
	go build -o a.out

test: clean dep
	( \
		go install; \
		go test; \
	)

check: test

go_release:
	GOOS=$(GOOS) GOARCH=amd64 go build -o course-deriver-$(GOOS)
	tar -cf - course-deriver-$(GOOS) | xz --stdout -9 - > course-deriver-$(GOOS).tar.xz

release: dep
	$(MAKE) GOOS=darwin go_release
	$(MAKE) GOOS=linux go_release
	$(MAKE) GOOS=windows go_release

clean:
	rm -rf a.out course-deriver-{windows,darwin,linux}*