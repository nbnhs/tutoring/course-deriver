# course-deriver

Formats a student's course history as JSON for easy usage with the peer tutoring system.

## Usage

`Usage: course-deriver [input file] [output file]`

## Supported Formats

- HTML (downloaded from the Skyward Academic History page)
- String (interpretation will be attempted)
