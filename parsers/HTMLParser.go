package parsers

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/nbnhs/tutoring/course-deriver/classes"
	"golang.org/x/net/html"
)

func getBody(doc *html.Node) (*html.Node, error) {
	var b *html.Node
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "body" {
			b = n
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	if b != nil {
		return b, nil
	}
	return nil, errors.New("Missing <body> in the node tree")
}

func renderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func getInnerHTML(node *html.Node) string {
	partA := strings.Split(renderNode(node), ">")[1]
	return strings.Split(partA, "<")[0]
}

// HTMLParser parses a Skyward Academic History page and returns a list of courses found on said page.
func HTMLParser(htmlText string) []classes.Course {
	doc, _ := html.Parse(strings.NewReader(htmlText))
	body, err := getBody(doc)

	if err != nil {
		log.Fatalln("HTML text input is not properly formatted!")
		os.Exit(2)
	}

	courses := make([]classes.Course, 1)
	// get to the actual content of the page
	coursesHTML := body.LastChild.LastChild.LastChild.LastChild.LastChild

	for c := coursesHTML.FirstChild; c != nil; c = c.NextSibling {
		if strings.Contains(renderNode(c), "font-weight:bold;line-height:1.3em") {
			// useless CSS line; skip past
			continue
		}

		name := getInnerHTML(c.FirstChild.FirstChild)
		course, err := StringParser(name)
		if err != nil {
			fmt.Println(err)
			continue
		}

		courses = append(courses, course)
	}

	return courses
}
