package parsers

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/nbnhs/tutoring/course-deriver/classes"
)

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// StringParser parses a single string with course info in the formats "Name Difficulty" "Difficulty Name" "Name Progression Difficulty" etc.
func StringParser(str string) (classes.Course, error) {
	arr := strings.Split(str, " ")
	var topic string
	var difficulty classes.Difficulty
	var progression uint8

	if arr[0] == "AP" || arr[0] == "Advanced" {
		// No progression for an AP class; AP is followed by topic name only
		tmpDifficulty, err := classes.ToDifficulty(arr[0])

		if err != nil {
			return classes.Course{}, errors.New("Couldn't find difficulty for " + str)
		}

		progression = 0
		difficulty = tmpDifficulty
		topic = strings.Join(arr[1:], " ")
	} else if arr[0] == "IB" {
		// IB follows special format
		difficultyProgressionArr := strings.Split(arr[len(arr)-1], "L") // e.g. HL1 becomes "H", "1"
		tmpDifficulty, err := classes.ToDifficulty(difficultyProgressionArr[0] + "L")

		if err != nil {
			return classes.Course{}, errors.New("Couldn't derive difficulty for IB course " + str)
		}

		tmp, err := strconv.ParseInt(difficultyProgressionArr[1], 10, 8) // e.g. HL1 becomes 1, SL2 becomes 2, etc.
		if err != nil {
			return classes.Course{}, errors.New("Couldn't derive year level for IB course " + str)
		}

		difficulty = tmpDifficulty
		progression = uint8(tmp)
		topic = strings.Join(arr[1:len(arr)-1], " ")
	} else {
		// Standard format used (e.g. "Name Difficulty" or "Name Progression Difficulty")
		// err value can be disregarded, since no last-item difficulty means difficulty is Regular/0 (default value)
		tmpDifficulty, _ := classes.ToDifficulty(arr[len(arr)-1])

		if len(arr) == 2 {
			// Name Difficulty
			topic = arr[0]
		} else {
			// Either "Name Progression Difficulty" or name is separated with spaces
			progressionIndex := -1
			for index, item := range arr {
				tmp, err := strconv.ParseInt(item, 10, 8)
				if err == nil {
					progression = uint8(tmp)
					progressionIndex = index
					break
				}
			}

			if progressionIndex != -1 {
				topic = strings.Join(arr[0:progressionIndex], " ")
			}
			topic = strings.Join(arr[0:len(arr)-1], " ")
		}

		difficulty = tmpDifficulty
	}

	if topic == "" {
		return classes.Course{}, errors.New("Couldn't parse course " + str)
	}

	return classes.Course{Topic: topic, Difficulty: difficulty, Progression: progression}, nil
}
