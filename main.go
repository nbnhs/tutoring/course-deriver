package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: course-deriver [input file] [output file]")
		os.Exit(1)
	}

	Switchboard(os.Args[1], os.Args[2])
}
