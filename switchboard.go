package main

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/nbnhs/tutoring/course-deriver/classes"
	"gitlab.com/nbnhs/tutoring/course-deriver/parsers"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readFile(path string) string {
	dat, err := ioutil.ReadFile(path)
	check(err)
	return string(dat)
}

func findLine(filePath string, textInLine string) string {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), textInLine) {
			return scanner.Text()
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return ""
}

func writeFile(path string, text []byte) {
	file, err := os.Create(path)
	check(err)

	file.Write([]byte(text))
}

// Switchboard directs an inputFile to the proper parser, then spits its output to the outputFile.
func Switchboard(inputFile string, outputFile string) {
	var parser func(string) []classes.Course // declare parser of type function (takes string, returns Course)
	parserText := ""

	if strings.Contains(inputFile, ".html") {
		parser = parsers.HTMLParser
		parserText = findLine(inputFile, "NBPS Middle/High School") // all text needed for parsing is on one line
	}

	if parserText == "" {
		parserText = readFile(inputFile)
	}

	data, err := json.Marshal(parser(parserText))
	check(err)

	writeFile(outputFile, data)
}
