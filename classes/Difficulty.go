package classes

import "errors"

// Difficulty represents the difficulty of a Course.
type Difficulty uint8

const (
	// Regular represents a CP/Regular level course.
	Regular Difficulty = 0
	// Honors represents an Honors level course.
	Honors Difficulty = 1
	// AP represents an AP course.
	AP Difficulty = 2
	// SL represents an IB SL course.
	SL Difficulty = 3
	// HL represents an IB HL course.
	HL Difficulty = 4
)

// ToDifficulty converts a string to a Difficulty value.
func ToDifficulty(str string) (Difficulty, error) {
	switch str {
	case "Honors":
		fallthrough
	case "Hon":
		return Honors, nil
	case "AP":
		fallthrough
	case "Advanced":
		return AP, nil
	case "SL":
		return SL, nil
	case "HL":
		return HL, nil
	case "Regular":
		return Regular, nil
	}
	return 0, errors.New("Did not understand str")
}
