package classes

// Course represents a course offered at North Broward.
type Course struct {
	Topic       string
	Difficulty  Difficulty
	Progression uint8
}
